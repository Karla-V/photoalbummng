﻿using Newtonsoft.Json;
using PhotoAlbumMng.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;


namespace PhotoAlbumMng.API_connection
{
    public class connection
    {
         
        
         
       ////this class is in charge of the concnection with the external API
        


        public User GetUser(string idUser)
        {
            string URL = "https://jsonplaceholder.typicode.com/";
            HttpClient client = new HttpClient();
            URL += "users/" + idUser;
            client.BaseAddress = new Uri(URL);
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));



            // List data response.
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var user = response.Content.ReadAsAsync<User>().Result;
                Console.WriteLine("{0}", user.name);
                return user;
                //foreach (var d in dataObjects)
                //{
                //    Console.WriteLine("{0}", d.name);
                //}
            }
            else
            {
               
                User user = new User();
                user.name = "no hay";

                return user;
            }

            
        }
        
        //Get the information of all users with the first photo of the album
        public List<User> GetAllUser()
        {
            string URL = "https://jsonplaceholder.typicode.com/";
            HttpClient client = new HttpClient();
            URL += "users/";
            client.BaseAddress = new Uri(URL);
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var user = response.Content.ReadAsAsync<List<User>>().Result;

                foreach (var u in user)
                {
                    string photo = GetFirstPhotoByUser(u.id.ToString());                                        
                    u.firstPhoto = photo;
                }



                return user;

            }
            else
            {

                User user = new User();
                user.name = "no hay";
                List<User> list = new List<User>();
                list.Add(user);
                return list;
            }


        }

        //get all albums from an user
        public List<Album> GetAlbumsByUser(string idUser)
        {
            string URL_albums = "https://jsonplaceholder.typicode.com/albums";
            HttpClient client = new HttpClient();
            URL_albums += "?userId="+idUser;

            client.BaseAddress = new Uri(URL_albums);
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var albums = response.Content.ReadAsAsync<List<Album>>().Result;
                //foreach (var a in albums)
                //{
                //    List<Photo> photos = GetPhotoByAlbum(a.id.ToString());
                //    a.photos = photos;
                //}

                return albums;

            }
            else
            {

                Album user = new Album();
                user.title = "no hay";
                List<Album> list = new List<Album>();
                list.Add(user);
                return list;
            }


        }

        //get all photos from an Album
        public List<Photo> GetPhotoByAlbum(string idAlbum)
        {
            string URL_photos = "https://jsonplaceholder.typicode.com/photos";
            HttpClient client = new HttpClient();
            URL_photos += "?albumId=" + idAlbum;

            client.BaseAddress = new Uri(URL_photos);
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var photos = response.Content.ReadAsAsync<List<Photo>>().Result;
                return photos;

            }
            else
            {

                Photo user = new Photo();
                user.title = "no hay";
                List<Photo> list = new List<Photo>();
                list.Add(user);
                return list;
            }


        }

        //get the first photo of an user
        public string GetFirstPhotoByUser(string idUser)
        {
            string URL_albums = "https://jsonplaceholder.typicode.com/albums";
            string URL_photos = "https://jsonplaceholder.typicode.com/photos";
            HttpClient client = new HttpClient();
            URL_albums += "?userId=" + idUser;

            client.BaseAddress = new Uri(URL_albums);
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var albums = response.Content.ReadAsAsync<List<Album>>().Result;
                Album al = albums.FirstOrDefault();
                HttpClient client2 = new HttpClient();
                URL_photos += "?albumId=" + al.id;

                client2.BaseAddress = new Uri(URL_photos);
                // Add an Accept header for JSON format.
                client2.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response2 = client2.GetAsync(client2.BaseAddress).Result;
                if (response2.IsSuccessStatusCode)
                {
                    var photo = response2.Content.ReadAsAsync<List<Photo>>().Result;
                    string photo_url = photo.FirstOrDefault().thumbnailUrl;

                    return photo_url;

                }
                else
                {
                    return "no Photo";
                }


            }
            else
            {

                return "no Album";
            }


        }

        //do the search of users by filter
        public List<User> SearchUsersByFilter(string filter, int opt)
        {
            List<User> userResult = new List<User>();
            List <User> userList = GetAllUser();

            if (opt==1)//name, username or email
            {
               List<User> userByName= userList.FindAll(x => x.name.Contains(filter));
                userResult = userByName;
                List<User> userByUsername = userList.FindAll(x => x.username.Contains(filter));
                foreach (var item in userByUsername)
                {
                    if (userResult.Find(x=>x.id.Equals(item.id))==null)//to not repeat users
                    {
                        userResult.Add(item);
                    }
                }
                List<User> userByEmail = userList.FindAll(x => x.email.Contains(filter));
                foreach (var item in userByEmail)
                {
                    if (userResult.Find(x => x.id.Equals(item.id)) == null)//to not repeat users
                    {
                        userResult.Add(item);
                    }
                }


            }
            if (opt==2)// city
            {
                userResult = userList.FindAll(x => x.address.city.Contains(filter));
                


            }

            if (userResult.Count == 0)
            {
                User user = new User();
                user.name = "Sin resultados";
                user.username = "Sin resultados";
                user.email = "Sin resultados";
                Address ad = new Address();
                ad.city= "Sin resultados";
                user.address = ad;
                userResult.Add(user);

            }

            return userResult;


        }

        //get the information of an album
        public Album GetAlbumsInfo(string idAlbum)
        {
            string URL_albums = "https://jsonplaceholder.typicode.com/albums";
            HttpClient client = new HttpClient();
            URL_albums += "?id=" + idAlbum;

            client.BaseAddress = new Uri(URL_albums);
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(client.BaseAddress).Result;
            if (response.IsSuccessStatusCode)
            {
                //var user = response.Content.ReadAsAsync<User>().Result;
                // Parse the response body. Blocking!
                var album = response.Content.ReadAsAsync<List<Album>>().Result;


                return album.First();

            }
            else
            {

                Album user = new Album();
                user.title = "no hay";
                
                return user;
            }


        }

        /// <summary>
        /// to update an album
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public bool UpdateAlbum(Album a)
        {
            var json = JsonConvert.SerializeObject(a);
            var buffer = System.Text.Encoding.UTF8.GetBytes(json);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");


            string URL_albums = "https://jsonplaceholder.typicode.com/albums/" + a.id;
            HttpClient client = new HttpClient();
           
            client.BaseAddress = new Uri(URL_albums);    
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

          
            HttpResponseMessage response = client.PutAsync(client.BaseAddress,byteContent).Result;
            if (response.IsSuccessStatusCode)
            {
             
                return true;

            }
            else
            {

                return false;
            }


        }
        /// <summary>
        /// to delete an album
        /// </summary>
        /// <param name="PhotoId"></param>
        /// <returns></returns>
        public bool DeletePhoto(int PhotoId)
        {
          

            string URL_photos = "https://jsonplaceholder.typicode.com/photos/" + PhotoId;
            HttpClient client = new HttpClient();

            client.BaseAddress = new Uri(URL_photos);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));


            HttpResponseMessage response = client.DeleteAsync(client.BaseAddress).Result;
            if (response.IsSuccessStatusCode)
            {
               


                return true;

            }
            else
            {

                return false;
            }


        }

        /// <summary>
        /// to add a photo
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool AddPhoto(Photo p)
        {
            var json = JsonConvert.SerializeObject(p);
            var buffer = System.Text.Encoding.UTF8.GetBytes(json);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");


            string URL_photos = "https://jsonplaceholder.typicode.com/photos/";
            HttpClient client = new HttpClient();

            client.BaseAddress = new Uri(URL_photos);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));


            HttpResponseMessage response = client.PostAsync(client.BaseAddress, byteContent).Result;
            if (response.IsSuccessStatusCode)
            {

                return true;

            }
            else
            {

                return false;
            }


        }




    }
}