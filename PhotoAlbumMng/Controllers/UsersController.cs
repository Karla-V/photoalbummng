﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PhotoAlbumMng.API_connection;
using PhotoAlbumMng.Models;


namespace PhotoAlbumMng.Controllers
{
    public class UsersController : Controller
    {
        /// <summary>
        /// NOTE:  For method of update, create and delete methods the API not updated, created, delete data just faked as if the data was altered
        /// </summary>



        private connection api = new connection();

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Albums(int id)
        {
            return View();
        }

        public ActionResult Photos(int id)
        {
            return View();
        }

        public ActionResult Update_Album(int id)
        {
            return View();
        }

        public ActionResult Add_Photo(int id)
        {
            return View();
        }


        //get all user information 
        [HttpPost]
        public JsonResult AjaxMethod()
        {
            //User u = api.GetUser("1");
            List<User> allUsers = new List<User>();
            allUsers = api.GetAllUser();

            return Json(allUsers);
        }

        //search users by the selected filter
        public JsonResult Search(string filter, int opt)
        {
          
            List<User> allUsers = new List<User>();
            allUsers = api.SearchUsersByFilter(filter,opt);

            return Json(allUsers);
        }

        //get all albums from an user. Parameter is idUser
        public JsonResult Load_Album(int id)
        {

            List<Album> allAlbum = new List<Album>();
            allAlbum = api.GetAlbumsByUser(id.ToString());

            return Json(allAlbum, JsonRequestBehavior.AllowGet);
        }

        //get all photos from an album. Parameter is idAlbum
        public JsonResult Load_Photo(int id)
        {

            List<Photo> allPhoto = new List<Photo>();
            allPhoto = api.GetPhotoByAlbum(id.ToString());

            return Json(allPhoto, JsonRequestBehavior.AllowGet);
        }

        //get the information of an album, Parameter is idAlbum
        public JsonResult Load_Album_Info(int id)
        {

            Album album = new Album();
            album = api.GetAlbumsInfo(id.ToString());

            return Json(album, JsonRequestBehavior.AllowGet);
        }

        //call method to update an album, true is OK false is not OK
        public JsonResult Set_Update_album(Album a)
        {

            bool resultado = api.UpdateAlbum(a);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        //Call method to delete a photo , true is OK(deleted) false is not OK(no deleted)
        public JsonResult Delete_Photo(int idPhoto)
        {

            bool resultado = api.DeletePhoto(idPhoto);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        //call method to add a photo , true is OK false is not OK
        public JsonResult Set_Add_Photo(Photo p)
        {

            bool resultado = api.AddPhoto(p);

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

    }
}