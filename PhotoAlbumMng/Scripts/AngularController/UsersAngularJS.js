﻿var app = angular.module('MyApp', [])
app.controller('UsersController', function ($scope, $http, $window) {
    $scope.ButtonClick = function () {
        $http({
            method: "POST",
            url: "/Users/AjaxMethod",
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            $scope.record = response.data;
        }, function (error) {
            $window.alert(error);
        });

    }
    $scope.ButtonSearch1_Click = function () {
        
        //console.log($scope.chk);

        $http({
            method: "POST",
            url: "/Users/Search",
            dataType: 'json',
            data: { filter: $scope.txtFilter, opt: $scope.chk },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            $scope.record = response.data;
        }, function (error) {
            $window.alert(error);
        });

    }
 
    $scope.loadAlbum = function (id) {
        //console.log(id);
        $http.get("/Users/Load_Album?id=" + id).then(function (d) {
            //console.log(d.data);
            $scope.register = d.data;

        }, function (error) {

            alert('Failed');

        });

    };

    $scope.loadPhoto = function (id) {
       // console.log(id);
        $http.get("/Users/Load_Photo?id=" + id).then(function (d) {
            //console.log(d.data[0].albumId);
            $scope.AlbumId = d.data[0].albumId;
            $scope.register = d.data;

        }, function (error) {

            alert('Failed');

        });

    };

    $scope.loadAlbumInfo = function (id) {
        //console.log(id);
        $http.get("/Users/Load_Album_Info?id=" + id).then(function (d) {
            //console.log(d.data);
            $scope.register = d.data;

        }, function (error) {

            alert('Failed');

        });

    };

    $scope.UpdateButtonClick = function () {
        $http({
            method: "PUT",
            url: "/Users/Set_Update_album",
            dataType: 'json',
            data: { id: $scope.register.id, title: $scope.register.title, userId: $scope.register.userId},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //console.log(response.data);
            if (response.data) {
                $window.alert("Album Updated Succesfully!!");
                setTimeout($window.location.href = '/Users/Index', 5000);
            } else {
                $window.alert("Album Update Fail");
            }
            
        }, function (error) {
            //    console.log(response.data);
            $window.alert(error);
        });

    }

    $scope.deletePhoto = function (id) {
        
        $http({
            method: "DELETE",
            url: "/Users/Delete_Photo",
            dataType: 'json',
            data: { idPhoto: id },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            //console.log(response.data);
            if (response.data) {
                $window.alert("Photo Deleted!!");
                setTimeout($window.location.href = '/Users/Index', 5000);
            } else {
                $window.alert("Photo Delete Fail");
            }

        }, function (error) {
            //    console.log(response.data);
            $window.alert(error);
        });

    };

    //call the view and past the albumId to the UsersController
    $scope.LoadAddPhotoForm = function () {

        $window.location.href = '/Users/Add_Photo?id=' + $scope.AlbumId;
        

    };

    //get the albumId from the UsersController and put it in the add_photo view
    $scope.LoadAddPhotoForm2 = function (id) {

        $scope.idAlbum = id;           

    };

    $scope.AddPhoto = function (id) {

        $http({
            method: "POST",
            url: "/Users/Set_Add_Photo",
            dataType: 'json',
            data: { albumId: $scope.idAlbum, id: $scope.txtId, title: $scope.txtTitle, url: $scope.txtUrl, thumbnailUrl: $scope.txtThumbnailUrl },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            if (response.data) {
                $window.alert("Photo Saved!!");
                setTimeout($window.location.href = '/Users/Index', 5000);
            } else {
                $window.alert("Photo Save Fail");
            }
        }, function (error) {
            $window.alert(error);
        });

    };

    //$scope.ButtonClick = function () {
    //    $http({
    //        method: "POST",
    //        url: "/Users/AjaxMethod",
    //        dataType: 'json',
    //        data: { _name: $scope.Name },
    //        headers: { "Content-Type": "application/json" }
    //    }).then(function (response) {
    //        console.log(response);
    //        console.log(response.data);
    //        $window.alert("Hello: " + response.data.name + " .\nCurrent Date and Time: " + response.data.username);
    //    }, function (data) {
    //        $window.alert(data.phone);
    //    });

    //}
});